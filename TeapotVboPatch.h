#ifndef TEAPOTVBOPATCH_H
#define TEAPOTVBOPATCH_H

#include "defines.h"
#include "teapotdata.h"

class GLSLShader;

class TeapotVboPatch
{
private:
    GLuint mTeapotVAOID;
    GLuint mTeapotBufferID;

    void generatePatches(float *v);
    void buildPatchReflect(int patchNum,
                           float *v,
                           int &index,
                           bool reflectX,
                           bool reflectY);

    void buildPatch(glm::vec3 patch[][4],
                    float *v,
                    int &index,
                    glm::mat3 reflect);

    void getPatch(int patchNum,glm::vec3 patch[][4],bool reverseV);

public:

    TeapotVboPatch(GLSLShader *);
    ~TeapotVboPatch();

    void render(GLSLShader *shader);

};

#endif // TEAPOTVBOPATCH_H
