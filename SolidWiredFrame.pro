TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += .
INCLUDEPATH += /usr/local/include/GLFW


LIBS += -lglfw -lGLEW -lGL -lGLU -lSOIL

SOURCES += main.cpp \
    TeapotVboPatch.cpp \
    GLSLShader.cpp

HEADERS += \
    TeapotVboPatch.h \
    teapotdata.h \
    GLSLShader.h \
    defines.h

