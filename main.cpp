#include "defines.h"
#include "GLSLShader.h"
#include "TeapotVboPatch.h"

int winWidth = 512;
int winHeight = 512;

glm::mat4 ModelviewMatrix = glm::mat4(1.0f);
glm::mat4 ProjectionMatrix = glm::mat4(1.0f);
glm::mat4 ViewportMatrix = glm::mat4(1.0f);
glm::mat3 NormalMatrix = glm::mat3(1.0f);

glm::mat4 ModelMatrix = glm::mat4(1.0f);
glm::mat4 ViewMatrix = glm::mat4(1.0f);


//hold the inner and
//outer tessellation level
float inner = 1.0f;
float outer = 1.0f;
float zoom = 22.f;

GLint maxPatchVertices = 0;

GLFWwindow *window = NULL;
GLSLShader *solidWireframeShader = NULL;
TeapotVboPatch *teapot = NULL;

static void error_callback(int error,const char* description);
static void framebuffer_size_callback(GLFWwindow *window,int width,int height);
static void scroll_callback(GLFWwindow *window,double x,double y);
static void key_callback(GLFWwindow *window,int key,int scancode,int action,int mods);

void startup();
void loadShaders();
void render(float);
void shutdown();

int main()
{
    // Initialise GLFW before using any of its function
    if( !glfwInit() )
    {
       std::cerr << "Failed to initialize GLFW." << std::endl;
       exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    //we want the opengl 4
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    //we do not want the old opengl
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( winWidth, winHeight, "Teapot Tessellation", NULL, NULL);

    if( window == NULL )
    {
       fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 4.3 compatible. Try the 2.1 version of the tutorials.\n" );

       //we could not initialize the glfw window
       //so we terminate the glfw
       glfwTerminate();
       exit(EXIT_FAILURE);
    }

    glfwSetErrorCallback(error_callback);
    glfwSetFramebufferSizeCallback(window,framebuffer_size_callback);
    glfwSetScrollCallback(window,scroll_callback);
    glfwSetKeyCallback(window,key_callback);


    //make the current window context the current one
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    glfwGetFramebufferSize(window,&winWidth,&winHeight);
    framebuffer_size_callback(window,winWidth,winHeight);

    //initialize glew
    //needed for the core profile
    glewExperimental  = true;

    if(glewInit() != GLEW_OK)
    {
        std::cerr << "Failed to initialize GLEW" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(!glewIsSupported("GL_VERSION_4_4"))
    {
        std::cerr << "OpenGL version 4.4 is yet to be supported" << std::endl;
        exit(EXIT_FAILURE);
    }

    while(glGetError() != GL_NO_ERROR) {}

    std::cout << "**************** OpenGL Driver Information *********************" << std::endl;

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GLEW version: " << glewGetString(GLEW_VERSION) << std::endl;
    std::cout << "OpenGL vendor: " << glGetString(GL_VENDOR) << std::endl;
    std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;

    std::cout << "****************************************************************" << std::endl;

    //call some initialization function
    startup();


    do
    {
        render((float)glfwGetTime());
        glfwSwapBuffers(window);
        glfwPollEvents();
    } while(glfwGetKey(window,GLFW_KEY_ESCAPE) != GLFW_PRESS &&
            glfwWindowShouldClose(window) == 0);

    //once the following function is called
    //no more events will be dilivered for that
    //window and its handle becomes invalid
    glfwDestroyWindow(window);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    //release all the resources that are allocated
    shutdown();

    exit(EXIT_SUCCESS);
}

void startup()
{
    loadShaders();

    glClearColor(0.5f,0.5f,0.5f,1.0f);

    teapot = new TeapotVboPatch(solidWireframeShader);

    glEnable(GL_DEPTH_TEST);

    //get the maximum patch vertices that this driver supportsf
    glGetIntegerv(GL_MAX_PATCH_VERTICES,&maxPatchVertices);

    glPatchParameteri(GL_PATCH_VERTICES,16);
}

void loadShaders()
{
    if(solidWireframeShader)
    {
        solidWireframeShader->DeleteShaderProgram();
        delete solidWireframeShader;
        solidWireframeShader = NULL;
    }

    solidWireframeShader = new GLSLShader();
    solidWireframeShader->LoadFromFile(GL_VERTEX_SHADER,"shaders/solidwiredframe.vert");
    solidWireframeShader->LoadFromFile(GL_TESS_CONTROL_SHADER,"shaders/solidwiredframe.tcs");
    solidWireframeShader->LoadFromFile(GL_TESS_EVALUATION_SHADER,"shaders/solidwiredframe.tes");
    solidWireframeShader->LoadFromFile(GL_GEOMETRY_SHADER,"shaders/solidwiredframe.geom");
    solidWireframeShader->LoadFromFile(GL_FRAGMENT_SHADER,"shaders/solidwiredframe.frag");

    solidWireframeShader->CreateAndLinkProgram();

    solidWireframeShader->Use();

    solidWireframeShader->AddUniform("inner");
    solidWireframeShader->AddUniform("outer");
    solidWireframeShader->AddUniform("ModelviewMatrix");
    solidWireframeShader->AddUniform("ProjectionMatrix");
    solidWireframeShader->AddUniform("NormalMatrix");
    solidWireframeShader->AddUniform("ViewportMatrix");


    solidWireframeShader->AddUniform("Line.Width");
    solidWireframeShader->AddUniform("Line.Color");

    solidWireframeShader->AddUniform("Light.Position");
    solidWireframeShader->AddUniform("Light.Intensity");

    solidWireframeShader->AddUniform("Material.Kd");
    solidWireframeShader->AddUniform("Material.Ka");
    solidWireframeShader->AddUniform("Material.Ks");
    solidWireframeShader->AddUniform("Material.Shininess");



    //set the uniform value here that remain
    //constant for the application lifetime
    glUniform1f((*solidWireframeShader)("Line.Width"),0.8f);

    //define the color of the line
    glm::vec4 LineColor = glm::vec4(0.05f,0.0f,0.05f,1.0f);

    //send the line color to the shader
    glUniform4fv((*solidWireframeShader)("Line.Color"),1,glm::value_ptr(LineColor));

    //define the light position
    glm::vec4 LightPosition = glm::vec4(0.25f,0.25f,1.0f,1.0f);

    glUniform4fv((*solidWireframeShader)("Light.Position"),1,glm::value_ptr(LightPosition));

    //define the light intensity
    glm::vec3 LightIntensity = glm::vec3(1.f,1.f,1.f);

    glUniform3fv((*solidWireframeShader)("Light.Intensity"),1,glm::value_ptr(LightIntensity));

    //now set and send the material property
    glm::vec3 MaterialAmbientReflectivity = glm::vec3(0.2f,0.2f,0.2f);

    glUniform3fv((*solidWireframeShader)("Material.Ka"),1,glm::value_ptr(MaterialAmbientReflectivity));


    glm::vec3 MaterialDiffuseReflectivity = glm::vec3(0.7f,0.7f,0.7f);

    glUniform3fv((*solidWireframeShader)("Material.Kd"),1,glm::value_ptr(MaterialDiffuseReflectivity));

    glm::vec3 MaterialSpecularReflectivity = glm::vec3(0.8f,0.8f,0.8f);

    glUniform3fv((*solidWireframeShader)("Material.Ks"),1,glm::value_ptr(MaterialSpecularReflectivity));

    float MaterialShininess = 100.0f;

    glUniform1f((*solidWireframeShader)("Material.Shininess"),MaterialShininess);

    solidWireframeShader->UnUse();
}

void render(float currentTime)
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 Ty = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,1.5f,0.0f));
    glm::mat4 Tz = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,0.0f,-zoom));
    glm::mat4 Ry = glm::rotate(glm::mat4(1.0),currentTime,glm::vec3(0.0f,1.0f,0.0f));
    glm::mat4 Rx = glm::rotate(glm::mat4(1.0f),20.0f,glm::vec3(1.0f,0.0f,0.0f));


    ModelMatrix = Tz * Ty * Ry * Rx;

    ModelviewMatrix =  ViewMatrix *  ModelMatrix;

    //calculate the normal matrix from the modelview matrix
    NormalMatrix = glm::inverseTranspose(glm::mat3(ModelviewMatrix));

    solidWireframeShader->Use();

    glUniformMatrix4fv((*solidWireframeShader)("ModelviewMatrix"),1,GL_FALSE,glm::value_ptr(ModelviewMatrix));
    glUniformMatrix4fv((*solidWireframeShader)("ProjectionMatrix"),1,GL_FALSE,glm::value_ptr(ProjectionMatrix));
    glUniformMatrix4fv((*solidWireframeShader)("ViewportMatrix"),1,GL_FALSE,glm::value_ptr(ViewportMatrix));
    glUniformMatrix3fv((*solidWireframeShader)("NormalMatrix"),1,GL_FALSE,glm::value_ptr(NormalMatrix));

    glUniform1f((*solidWireframeShader)("inner"),inner);
    glUniform1f((*solidWireframeShader)("outer"),outer);

    teapot->render(solidWireframeShader);

    solidWireframeShader->UnUse();
}

void shutdown()
{
    if(solidWireframeShader)
    {
        solidWireframeShader->DeleteShaderProgram();
        delete solidWireframeShader;
        solidWireframeShader = NULL;
    }

    if(teapot)
    {
        delete teapot;
        teapot = NULL;
    }
}

void error_callback(int error,const char* description)
{

}

void framebuffer_size_callback(GLFWwindow *window,int width,int height)
{

    if(height < 1)
        height = 1;

    winWidth = width;
    winHeight = height;


    //set the viewport matrix
    ViewportMatrix = glm::mat4(glm::vec4((GLfloat)winWidth/2.0f,0.0f,0.0f,0.0f),
                               glm::vec4(0.0f,(GLfloat)winHeight/2.0f,0.0f,0.0f),
                               glm::vec4(0.0f,0.0f,1.0f,0.0f),
                               glm::vec4((GLfloat)winWidth/2.0f,(GLfloat)winHeight/2.0f,0.0f,1.0f));

    GLfloat aspect = winWidth/(GLfloat)winHeight;

    //setup the projection matrix
    ProjectionMatrix = glm::perspective(50.0f,aspect,0.1f,1000.0f);

    glViewport(0,0,winWidth,winHeight);
}

void scroll_callback(GLFWwindow *window,double x,double y)
{
    //increase/decrease the field of view
    zoom += (float)y/4.f;

    if(zoom < 0)
        zoom = 0;
}

void key_callback(GLFWwindow *window,int key,int scancode,int action,int mods)
{
    if(action != GLFW_PRESS) return;

    switch(key)
    {
        case GLFW_KEY_UP:
            outer = outer <= maxPatchVertices ? outer + 1 : maxPatchVertices;
            break;
        case GLFW_KEY_DOWN:
            outer = outer > 1 ? outer -1 : 1;
            break;
        case GLFW_KEY_RIGHT:
            inner = inner <= maxPatchVertices ? inner + 1 : maxPatchVertices;
            break;
        case GLFW_KEY_LEFT:
            inner = inner > 1 ? inner -1 : 1;
            break;
        case GLFW_KEY_R:
            outer = 1;
            inner = 1;
            break;
        default:
            break;
    }
}

